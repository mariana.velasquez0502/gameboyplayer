var count=1;

class AudioPlayer{
    constructor(src,controls,info){
        this.src=src[count-1];
        this.audio= new Audio(this.src);
        this.audio.volume=0.2;
        this.controls=controls;
        this.isPaused=true;
        this.progress=controls.progress;
        this.audio.ontimeupdate = () => {
            this.updateUI();
        }
        this.initControls();
        this.nextSong(src,controls,info);
    }

    initControls(){
        if (this.controls.play){
            this.initPlay(this.controls.play);
        }      
        this.volumen(controls);  
        this.time(controls);
       
    }

    initPlay(domElement){
        domElement.onclick = () =>{
            // console.log(this.isPaused);
            if (!this.isPaused){
                this.pause();
                this.isPaused=true;
            }else{
                this.play();
                this.isPaused=false;
            }
        }
    }

    updateUI(){
        // console.log("Updating IU");
        const total=this.audio.duration;
        const current=this.audio.currentTime;
        const progress = current/total*100;
        this.progress.style.width=`${progress}%`;
    }

    play(){
        this.audio.play().then().catch(err => console.log(`Error al reproducir el archivo: ${err}`));
    }

    pause(){
        this.audio.pause();
    }

    volumen(controls){
        controls.volumenUP.onclick= () => {
            if(this.audio.volume<1){
                this.audio.volume+=0.1;
                // console.log(this.audio.volume);
            }            
        }

        controls.volumenDown.onclick= () =>{
            if(this.audio.volume>0.1){
               this.audio.volume-=0.1;
            //    console.log(this.audio.volume);
            }  
        }   
    }

    time(controls){
        controls.forWard.onclick=() => {
            this.audio.currentTime+=10;
            // console.log(this.audio.currentTime);
        }

        controls.backWard.onclick=() => {
            this.audio.currentTime-=10;
            // console.log(this.audio.currentTime);
        }
    }

    nextSong(src,controls,info){
        controls.nextBtn.onclick= () =>{

            if(!this.isPaused){

                this.audio.pause();

                if(count>2){
                    count=0;
                }

                this.song=src[count++];
                this.audio= new Audio(this.song);
                this.audio.play();
                this.audio.ontimeupdate= () => {this.updateUI();}

                var img = document.getElementById("coverImg");
                img.src = './assets/covers/'+count+'.jpg';

                document.getElementById("Info").innerHTML = info[count-1];
                               
                
            }
        }
    }
}